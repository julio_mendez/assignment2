
import java.io.*;
import java.util.*;
import java.net.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyEavesdropServlet
 */
@WebServlet("/MyEavesdropServlet")
public class MyEavesdropServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final int urlData = 1;
	private static final int userSession = 2;
	private static final int error = -1;
	private URL url = null;

	/**
	 * Default constructor. 
	 */
	public MyEavesdropServlet() {
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		String query = request.getQueryString();
		String localHostURL = request.getRequestURL().toString() + "?" + query;
		Cookie cookies[] = request.getCookies();
		Map<String, String> params = new HashMap<String, String>();
		String[] args = query.split("&"); 
		
		if(args.length == 2 || args.length == 3) {
			parser(args, params);
			int flag = validateArgs(params);
			try
			{
				switch(flag) {
					case urlData: 
						String source;
						if(params.get("type").equals("meetings")) {
						source = "http://eavesdrop.openstack.org/" + params.get("type") + "/" + 
								params.get("project") + "/" + params.get("year");
							
						}						
						else {
							source = "http://eavesdrop.openstack.org/" + params.get("type") + "/" + 
									params.get("project");
						}
						
						try 
						{
							url = new URL(source);
						} catch (MalformedURLException e) {
							e.printStackTrace();
							System.exit(0);
						}
						
						try {
							HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							
							StringBuilder sb = new StringBuilder();
							InputStream input = connection.getInputStream();
							Scanner scan = new Scanner(input);
							while (scan.hasNext()) {
								sb.append(scan.next());
								sb.append("");
							}
							String sourceCode = sb.toString();
							
							printURLS(cookies, params, localHostURL, request, response, pw);
	                        
			                pw.println("</br>URL data");
							pw.println(sourceCode);
							
							scan.close();
							input.close();
							connection.disconnect();
							
						} catch (FileNotFoundException e) {
							printURLS(cookies, params, localHostURL, request, response, pw);
							pw.println("</br>URL data");
						}
					break;
					
					case userSession: 
						if(params.get("session").equals("end")) {
			                if(cookies != null) {
				                for(int i=0; i<cookies.length; i++) {
			                        Cookie ck = cookies[i];
			                        ck.setMaxAge(0);
			                        ck.setDomain("");
			                        ck.setPath("/");
			                        ck.setValue(null);
			                        response.addCookie(ck);
				                }	                	
			                }
			                pw.println("<h1>Session end for " + params.get("username") + "!</h1>");
						}
						
						else {
							pw.println("<h1>Session start for " + params.get("username") + "!</h1>");
						}
					break;
					
					case error: 
						pw.println("<h1>ERROR 400 Bad Request</h1>");
					break;
					
					default: break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		else {
			pw.println("<h1>ERROR 400 Bad Request</h1>");
		}
	}
	
	private static void parser(String[] args, Map<String, String> params) {
		for(String parameter : args) {
			String[] p = parameter.split("=");
			params.put(p[0].toLowerCase(), p[1].toLowerCase());
		}
	}
	
	private static int validateArgs(Map<String, String> params) {
		if(params.get("type") != null) {
			if(!params.get("type").equals("meetings") && !params.get("type").equals("irclogs")) {
				return -1;
			}
			
			if(params.get("project") == null) {
				return -1;
			}
			
			if(!params.get("type").equals("irclogs") && 
					params.get("year") != null &&
					!params.get("year").equals("2010") && 
					!params.get("year").equals("2011") &&
					!params.get("year").equals("2012") &&
					!params.get("year").equals("2013") &&
					!params.get("year").equals("2014") &&
					!params.get("year").equals("2015")) {
				return -1;
			}
			
			return 1;
		}
		
		if(params.get("username") != null) {
			if(!params.get("session").equals("start") && !params.get("session").equals("end")) {
				return -1;
			}
			return 2;
		}
		
		else {
			return -1;
			
		}
		
	}
	
	private static void printURLS(Cookie[] cookies, Map<String, String> params, String localHostURL,
			HttpServletRequest request, HttpServletResponse response, PrintWriter pw) {
		Cookie cookie;
		
		pw.println("Visited URLs");
        if(cookies != null) {
            for(int i=0; i<cookies.length; i++) {
            	pw.println("</br>");
                Cookie ck = cookies[i];
                String cookieValue = ck.getValue();
                pw.println(cookieValue);
            }
        }
        
        String cookieName = params.get("type") + params.get("project") + params.get("year");
        cookie = new Cookie(cookieName, localHostURL);
        cookie.setDomain("localhost");
        cookie.setPath("/assignment2" + request.getServletPath());
        cookie.setMaxAge(1000);
        response.addCookie(cookie);
	}
}









